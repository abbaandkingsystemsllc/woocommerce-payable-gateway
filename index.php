<?php
/*
  Plugin Name: WooCommerce Payable Payment Gateway
  Plugin URI: http://www.payable.com.ng
  Description: Payable Payment gateway for woocommerce
  Version: 1.0
  Author: Odewumi Babarinde Ayodeji
  Author URI: http://www.payable.com.ng
 */
add_action('plugins_loaded', 'woocommerce_payable_init', 0);

function woocommerce_payable_init() {
    if (!class_exists('WC_Payment_Gateway'))
	return;

    class WC_Payable extends WC_Payment_Gateway {

	public function __construct() {
	    $this->id = 'payable';
	    $this->medthod_title = 'Payable';
	    $this->has_fields = false;
	    $this->order_button_text    = __( 'Proceed to Payable', 'woocommerce' );

	    $this->init_form_fields();
	    $this->init_settings();

	    $this->title = $this->settings['title'];
	    $this->description = $this->settings['description'];
	    $this->environment = $this->settings['environment'];
	    $this->merchant_id = $this->settings['merchant_id'];
	    $this->testing_merchant_id = $this->settings['testing_merchant_id'];
	    $this->redirect_page_id = $this->settings['redirect_page_id'];
	    $this->liveurl = 'https://www.payable.com.ng/merchant-transaction/process';
	    $this->testingurl = 'https://sandbox.payable.com.ng/merchant-transaction/process';

	    $this->msg['message'] = "";
	    $this->msg['class'] = "";

	    if (version_compare(WOOCOMMERCE_VERSION, '2.0.0', '>=')) {
		add_action('woocommerce_update_options_payment_gateways_' . $this->id, array(&$this, 'process_admin_options'));
	    } else {
		add_action('woocommerce_update_options_payment_gateways', array(&$this, 'process_admin_options'));
	    }
	    add_action('woocommerce_receipt_payable', array(&$this, 'receipt_page'));
	    add_action('woocommerce_api_' . strtolower(get_class($this)), array($this, 'check_payable_response'));

// Check if the gateway can be used
	    if (!$this->is_valid_for_use()) {
		$this->enabled = false;
	    }
	}

	function is_valid_for_use() {
	    if (!in_array(get_woocommerce_currency(), array('NGN'))) {
		$this->msg = 'Payable doesn\'t support your store currency, set it to Nigerian Naira &#8358; <a href="' . get_bloginfo('wpurl') . '/wp-admin/admin.php?page=wc-settings&tab=general">here</a>';
		return false;
	    }
	    return true;
	}

	function init_form_fields() {

	    $this->form_fields = array(
		'enabled' => array(
		    'title' => __('Enable/Disable', 'payable'),
		    'type' => 'checkbox',
		    'label' => __('Enable Payable Payment Module.', 'payable'),
		    'default' => 'no'),
		'title' => array(
		    'title' => __('Title:', 'payable'),
		    'type' => 'text',
		    'description' => __('This controls the title which the user sees during checkout.', 'payable'),
		    'default' => __('Payable', 'payable')),
		'description' => array(
		    'title' => __('Description:', 'payable'),
		    'type' => 'textarea',
		    'description' => __('This controls the description which the user sees during checkout.', 'payable'),
		    'default' => __('Pay in Naira by Debit card or Wallet through Payable Secure Servers.', 'payable')),
		'environment' => array(
		    'title' => __('Environment', 'payable'),
		    'type' => 'select',
		    'options' => array(
			'testing' => 'Testing',
			'live' => 'Live'),
		    'description' => __('Testing (sandbox.payable.com.ng) or Live (www.payable.com.ng)')),
		'testing_merchant_id' => array(
		    'title' => __('Testing API Key', 'payable'),
		    'type' => 'text',
		    'description' => __('The Testing API Key at sandbox.payable.com.ng.')),
		'merchant_id' => array(
		    'title' => __('LIVE API Key', 'payable'),
		    'type' => 'text',
		    'description' => __('The Live API Key at www.payable.com.ng.')),
	    );
	}

	public function admin_options() {
	    echo '<h3>' . __('Payable Payment Gateway', 'payable') . '</h3>';
	    echo '<p>' . __('Payable is most popular payment gateway for online shopping in Nigeria') . '</p>';
	    if ($this->is_valid_for_use()) {
		echo '<table class="form-table">';
		// Generate the HTML For the settings form.
		$this->generate_settings_html();
		echo '</table>';
	    } else {
		?>
		<div class="inline error"><p><strong>Payable Payment Gateway Disabled</strong>: <?php echo $this->msg ?></p></div>

		<?php
	    }
	}

	/**
	 *  There are no payment fields for payable, but we want to show the description if set.
	 * */
	function payment_fields() {
	    if ($this->description)
		echo wpautop(wptexturize($this->description));
	}

	/**
	 * Receipt Page
	 * */
	function receipt_page($order) {
	    echo '<p>' . __('Thank you for your order, please click the button below to pay with Payable.', 'payable') . '</p>';
	    echo $this->generate_payable_form($order);
	}

	/**
	 * Generate payable button link
	 * */
	public function generate_payable_form($order_id) {

	    global $woocommerce;
	    $order = new WC_Order($order_id);
	    $txnid = $order_id . '_' . date("ymds");

	    $productinfo = "Order $order_id";

	    $merchantId = ($this->environment == 'testing') ? $this->testing_merchant_id : $this->merchant_id;
	    $payu_args = array(
		'apiKey' => $merchantId,
		'parentTransactionId' => $txnid,
		'amount' => $order->order_total,
		'description' => $productinfo,
		'customerName' => $order->billing_first_name . ' ' . $order->billing_last_name,
		'currency' => 'Naira',
		'siteRedirectUrl' => get_site_url() . '?wc-api=wc_payable',
		'email' => $order->billing_email,
	    );

	    $payu_args_array = array();
	    foreach ($payu_args as $key => $value) {
		$payu_args_array[] = "<input type='hidden' name='$key' value='$value'/>";
	    }
	    $formDestination = ($this->environment == 'testing') ? $this->testingurl : $this->liveurl;
	    return '<form action="' . $formDestination . '" method="post" id="payable_payment_form">
            ' . implode('', $payu_args_array) . '
            <input type="submit" class="button-alt" id="submit_payable_payment_form" value="' . __('Pay via Payable', 'payable') . '" /> <a class="button cancel" href="' . $order->get_cancel_order_url() . '">' . __('Cancel order &amp; restore cart', 'payable') . '</a>
            <script type="text/javascript">
jQuery(function(){
jQuery("body").block(
        {
            message: "<img src=\"' . $woocommerce->plugin_url() . '/assets/images/ajax-loader.gif\" alt=\"Redirecting…\" style=\"float:left; margin-right: 10px;\" />' . __('Thank you for your order. We are now redirecting you to Payment Gateway to make payment.', 'payable') . '",
                overlayCSS:
        {
            background: "#fff",
                opacity: 0.6
    },
    css: {
        padding:        20,
            textAlign:      "center",
            color:          "#555",
            border:         "3px solid #aaa",
            backgroundColor:"#fff",
            cursor:         "wait",
            lineHeight:"32px"
    }
    });
    jQuery("#submit_payable_payment_form").click();});</script>
            </form>';
	}

	/**
	 * Process the payment and return the result
	 * */
	function process_payment($order_id) {
	    global $woocommerce;
	    $order = new WC_Order($order_id);
	    return array('result' => 'success', 'redirect' => add_query_arg('order', $order->id, add_query_arg('key', $order->order_key, get_permalink(woocommerce_get_page_id('pay'))))
	    );
	}

	/**
	 * Check for valid payable server callback
	 * */
	function check_payable_response() {
	    global $woocommerce;
	    if (isset($_REQUEST['parentTransactionId']) && isset($_REQUEST['payableId'])) {
		$orderId = explode('_', $_REQUEST['parentTransactionId']);
		$order_id = (int) $orderId[0];
		if ($order_id != '') {
		    try {
			$order = new WC_Order($order_id);

			$status = $_REQUEST['result'];

			$transauthorised = false;

			if ($order->status !== 'completed') {
			    if ($status == 1) {

				if ($order->status == 'processing') {
				    
				} else {
				    $order->payment_complete();
				    $order->add_order_note('Payable payment successful<br/>Unique Id from Payable: ' . $_REQUEST['payableId']);
				    $woocommerce->cart->empty_cart();
				}
				wp_redirect($this->get_return_url($order));
				exit();
			    } else {
				$order->add_order_note('Transaction Declined: ' . $_REQUEST['message']);
				wp_redirect(get_permalink(wc_get_page_id('cart')));
				exit();
			    }
			}
		    } catch (Exception $e) {
			wp_redirect(get_permalink(wc_get_page_id('cart')));
			exit();
		    }
		}
	    }
	}

	// get all pages
	function get_pages($title = false, $indent = true) {
	    $wp_pages = get_pages('sort_column=menu_order');
	    $page_list = array();
	    if ($title)
		$page_list[] = $title;
	    foreach ($wp_pages as $page) {
		$prefix = '';
		// show indented child pages?
		if ($indent) {
		    $has_parent = $page->post_parent;
		    while ($has_parent) {
			$prefix .= ' - ';
			$next_page = get_page($has_parent);
			$has_parent = $next_page->post_parent;
		    }
		}
		// add to page list array array
		$page_list[$page->ID] = $prefix . $page->post_title;
	    }
	    return $page_list;
	}

    }

    /**
     * Add the Gateway to WooCommerce
     * */
    function woocommerce_add_payable_gateway($methods) {
	$methods[] = 'WC_Payable';
	return $methods;
    }

    add_filter('woocommerce_payment_gateways', 'woocommerce_add_payable_gateway');
}
